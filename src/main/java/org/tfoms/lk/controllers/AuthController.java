package org.tfoms.lk.controllers;

import com.objsys.asn1j.runtime.Asn1Exception;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.tfoms.lk.model.auth.TokenJWT;
import org.tfoms.lk.model.auth.TokenWrap;
import org.tfoms.lk.security.Signer;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

/**
 * Авторизация пользователей в есиа
 * */
@Controller
@RequestMapping("/api")
public class AuthController {
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    private static final String AUTH_URL_FORMAT = "%s?%s";

    private static final String AUTH_PARAMS = "client_id=%s&" +
            "client_secret=%s&" +
            "redirect_uri=%s&" +
            "scope=%s&" +
            "response_type=%s&" +
            "state=%s&" +
            "timestamp=%s";

    private static final String STATE_ERROR_MGS = "State не совпадает. Сервис авторизации может быть не доступен. Попробуйте позже.";


    @Value("${esia.url.auth}")
    private String AUTH_URL;

    @Value("${esia.url.te}")
    private String TOKEN_EXCHANGE_URL;

    @Value("${lk.url.redirect}")
    private String redirectUri;

    @Value("${lk.clientid}")
    private String CLIENT_ID;

    @Value("${lk.auth.scopes}")
    private String SCOPE;

    @Value("${esia.url.logout}")
    private String LOGOUT_URL;

    private String RESPONSE_TYPE = "code";

    private String TOKEN_TYPE= "Bearer";

    private String GRANT_TYPE= "authorization_code";



    @Autowired
    private SimpleDateFormat format;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Signer signer;

    /**
     * Создает ссылку для редиректа на есиа
     * @return ссылка на есиа со всеми параметрами
     * */
    @GetMapping("/auth")
    public String auth(HttpServletRequest request)
            throws IOException, Asn1Exception, NoSuchAlgorithmException, CertificateEncodingException, SignatureException, InvalidKeyException {
        String timestamp = format.format(new Date());
        String state = UUID.randomUUID().toString();
        request.getSession().setAttribute("state",state);

        /*
         * – подпись запроса в формате PKCS#7 detached signature в кодировке UTF8
         * от значений четырех параметров HTTP–запроса: scope, timestamp, clientId, state
         * (без разделителей).
         * */
        String dataToSign = SCOPE + timestamp + CLIENT_ID + state;
        byte[] toBeSigned = dataToSign.getBytes("UTF-8");
        byte[] sign = signer.signData3(toBeSigned);

        String client_secret = Base64.getUrlEncoder().encodeToString(sign);


        //------------------------------------------------------------------------------ sign end

        //а ля url encode
        timestamp = timestamp.replace("+","%2B");

        String params = String.format(AUTH_PARAMS,CLIENT_ID,client_secret,redirectUri,SCOPE,RESPONSE_TYPE,state,timestamp);

        return "redirect:" + String.format(AUTH_URL_FORMAT,AUTH_URL,params);
    }

    /**
     * После авторизации, ЕСИА отправляет пользователя сюда
     * Обмен авторизационного кода на маркер доступа
     * @param code авторизационный код, полученный от есиа
     * @param state state, полученный от есиа
     * @return название представления
     * */
    @RequestMapping("/te")
    public String tokenExchange(@RequestParam String code, @RequestParam String state, HttpServletRequest request, Model model)
            throws IOException, Asn1Exception, NoSuchAlgorithmException, CertificateEncodingException, SignatureException, InvalidKeyException {
        HttpSession session = request.getSession();
        session.removeAttribute("access_token");

        String expectedState = (String) session.getAttribute("state");
        if(expectedState == null || !expectedState.equals(state)){
            model.addAttribute("description", STATE_ERROR_MGS);
            return "error";
        }

        state = UUID.randomUUID().toString();
        String timestamp = format.format(new Date());

        /*
         * – подпись запроса в формате PKCS#7 detached signature в кодировке UTF8
         * от значений четырех параметров HTTP–запроса: scope, timestamp, clientId, state
         * (без разделителей).
         * */
        String dataToSign = SCOPE + timestamp + CLIENT_ID + state;
        byte[] toBeSigned = dataToSign.getBytes("UTF-8");
        byte[] sign = signer.signData3(toBeSigned);

        String client_secret = Base64.getUrlEncoder().encodeToString(sign);

        //создаем тело запроса
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("client_id",CLIENT_ID);
        bodyMap.add("code",code);
        bodyMap.add("grant_type",GRANT_TYPE);
        bodyMap.add("client_secret",client_secret);
        bodyMap.add("state",state);
        bodyMap.add("redirect_uri",redirectUri);
        bodyMap.add("scope",SCOPE);
        bodyMap.add("timestamp",timestamp);
        bodyMap.add("token_type",TOKEN_TYPE);

        HttpEntity<MultiValueMap<String,String>> httpEntity = new HttpEntity<>(bodyMap, headers);


        TokenWrap response = new TokenWrap();
        //меняем код авторизации на JWT токен
        try {
            response = restTemplate.postForObject(TOKEN_EXCHANGE_URL, httpEntity, TokenWrap.class);
            if(!response.getState().equals(state)) {
                model.addAttribute("description", STATE_ERROR_MGS);
                return "error";
            }
        } catch (HttpClientErrorException e){
            logger.error("Token exchange error: {}", e.getMessage());
            model.addAttribute("description", e.getMessage());
            return "error";
        }


        TokenJWT tokenJWT = TokenJWT.getToken(response.getAccess_token());

        tokenJWT.setRefreshToken(response.getRefresh_token());

        //рассчитываем время истечения токена
        Date timeExpires = new Date();
        timeExpires.setTime(timeExpires.getTime() + response.getExpires_in() * 1000);
        tokenJWT.setExpires(timeExpires);

        //сохраняем токен в сессии
        session.setAttribute("access_token",tokenJWT);
        session.removeAttribute("state");


        return "redirect:/info";
    }

    @RequestMapping("/logout")
    public String logout(HttpServletRequest request){
        HttpSession session = request.getSession();
        session.removeAttribute("access_token");

        return  "redirect:" + LOGOUT_URL + "?client_id=" + CLIENT_ID + "&redirect_url=https://novofoms.ru";
    }

}
