package org.tfoms.lk.export;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.tfoms.lk.model.Treatment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ExcelBuilderTest {

    List<Treatment> treats = new ArrayList<>();


    @BeforeEach
    void setUp(){
        Treatment t = new Treatment();
        t.setCareRegimen("care regimen");
        t.setClinicName("clinic");
        t.setDateRenderingFrom("01-01-1999");
        t.setDateRenderingTo("01-01-2000");
        t.setMedServicesSum(123123.0);
        t.setName("name");

        treats.add(t);
    }

    @Test
    void treatmentToExcel() throws IOException {
        FileOutputStream fout = new FileOutputStream("test.xls");

        ExcelBuilder.treatmentToExcel(treats,fout);
    }
}