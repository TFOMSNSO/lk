package org.tfoms.lk.model;

public class Insurance {
    private String serPol;
    private String numPol;
    private String smo;
    private String typePol;
    private String dateBegin;
    private String dateEnd;
    private String lpu;
    private String smoPhone;
    private String numDoc;
    private String serDoc;
    private String docStr;
    private Integer docid;
    private Integer lpuId;
    private String addrStr;


    @Override
    public String toString() {
        return "Insurance{" +
                "serPol='" + serPol + '\'' +
                ", numPol='" + numPol + '\'' +
                ", smo='" + smo + '\'' +
                ", typePol='" + typePol + '\'' +
                ", dateBegin='" + dateBegin + '\'' +
                ", dateEnd='" + dateEnd + '\'' +
                ", lpu='" + lpu + '\'' +
                ", smoPhone='" + smoPhone + '\'' +
                ", numDoc='" + numDoc + '\'' +
                ", serDoc='" + serDoc + '\'' +
                ", docStr='" + docStr + '\'' +
                ", docid=" + docid +
                ", lpuId=" + lpuId +
                ", addrStr='" + addrStr + '\'' +
                '}';
    }

    public String getAddrStr() {
        return addrStr;
    }

    public void setAddrStr(String addrStr) {
        this.addrStr = addrStr;
    }

    public Integer getLpuId() {
        return lpuId;
    }

    public void setLpuId(Integer lpuId) {
        this.lpuId = lpuId;
    }

    public Integer getDocid() {
        return docid;
    }

    public void setDocid(Integer docid) {
        this.docid = docid;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public String getSerDoc() {
        return serDoc;
    }

    public void setSerDoc(String serDoc) {
        this.serDoc = serDoc;
    }

    public String getDocStr() {
        return docStr;
    }

    public void setDocStr(String docStr) {
        this.docStr = docStr;
    }

    public String getSmoPhone() {
        return smoPhone;
    }

    public void setSmoPhone(String smoPhone) {
        this.smoPhone = smoPhone;
    }

    public String getSerPol() {
        return serPol;
    }

    public void setSerPol(String serPol) {
        this.serPol = serPol;
    }

    public String getNumPol() {
        return numPol;
    }

    public void setNumPol(String numPol) {
        this.numPol = numPol;
    }

    public String getSmo() {
        return smo;
    }

    public void setSmo(String smo) {
        this.smo = smo;
    }

    public String getTypePol() {
        return typePol;
    }

    public void setTypePol(String typePol) {
        this.typePol = typePol;
    }

    public String getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(String dateBegin) {
        this.dateBegin = dateBegin;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getLpu() {
        return lpu;
    }

    public void setLpu(String lpu) {
        this.lpu = lpu;
    }
}
