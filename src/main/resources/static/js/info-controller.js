$(document).ready(function () {
    $('#dialog').dialog({
        autoOpen:false,
        modal: true,
        show:{
            effect: "blind",
            duration: 1000
        },
        hide:{
            effect: "explode",
            duration: 1000
        },
        buttons: {
            "Узнать":{
                click:function () {
                    $('#treat-form').submit();
                    if($('#treat-form').valid()){

                        
                        $('#progress-bar').progressbar({
                            value: false
                        });
                    }
                },
                class: 'btn btn-primary',
                text: 'Узнать'
            },
            "Закрыть": {
                click: function () {
                    $(this).dialog('close');
                },
                class: 'btn btn-secondary',
                text: 'Закрыть'
            }
        }
    });

    $('#accept-dialog').dialog({
        autoOpen: false,
        modal: true,
        height: "auto",
        width: 400,
        buttons: {
            "Скачать согласие": {
                click: function () {
                    window.location.href = '/agreement.doc';
                    $(this).dialog('close');
                },
                class: 'btn btn-primary',
                text: 'Скачать согласие'
            },
            "Закрыть" : {
                click: function () {
                    $(this).dialog('close');
                },
                class: 'btn btn-secondary',
                text: 'Закрыть'
            }
        },
        show:{
            effect: "blind",
            duration: 1000
        },
        hide:{
            effect: "explode",
            duration: 1000
        }
    });



    $('#treat-form').validate({
        rules:{
            dateBegin: {
                required: true
            },
            dateEnd: {
                required: true
            }
        },
        messages:{
            dateBegin: "Выберите дату начала",
            dateEnd: "Выберите дату окончания"
        }
    });

    $('#about-agreement').click(function () {
        $('#accept-dialog').dialog('open');
    });


    $('#about_treat').click(function (){
        $('#dialog').dialog('open');
    });



    $('#appeal').click(function () {
        $('#form_appeal').submit();
    });



    $('#check_polis').click(function () {
        $('#check').submit();
    });


    $('#agr-download').click(function () {
        $("#accept-dialog").dialog('close');
    })

    $('#ttip').tooltip();
});


