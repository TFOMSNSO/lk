package org.tfoms.lk.model.rest;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact {
    private String[] stateFacts;
    private Long id;
    private String type;
    private String vrfStu;
    private String value;
    private String eTag;

    @Override
    public String toString() {
        return "Contact{" +
                "stateFacts=" + Arrays.toString(stateFacts) +
                ", id=" + id +
                ", type='" + type + '\'' +
                ", vrfStu='" + vrfStu + '\'' +
                ", value='" + value + '\'' +
                ", eTag='" + eTag + '\'' +
                '}';
    }

    public String[] getStateFacts() {
        return stateFacts;
    }

    public void setStateFacts(String[] stateFacts) {
        this.stateFacts = stateFacts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVrfStu() {
        return vrfStu;
    }

    public void setVrfStu(String vrfStu) {
        this.vrfStu = vrfStu;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String geteTag() {
        return eTag;
    }

    public void seteTag(String eTag) {
        this.eTag = eTag;
    }
}
