package org.tfoms.lk.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tfoms.lk.model.MoSchedule;


import java.lang.reflect.Executable;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MoScheduleServiceTest {
    @Autowired
    MoScheduleService service;

    @Test
    public void shouldNotBeenThrown(){
        assertDoesNotThrow(() -> {
         service.getForId(null);
         service.getForId(111111111);
         service.getForId(-15);
        });
    }
    /*
    @Test
    public void shouldMatchFileData(){
        assertTrue(service.schedules.size() > 0);

        assertTrue(service.schedules.entrySet().size() == 65);

        for(Map.Entry<Integer,List<MoSchedule>> entry: service.schedules.entrySet()) {
            int id = entry.getKey();
            List<MoSchedule> data = entry.getValue();
            MoSchedule first = data.get(0);
            assertTrue(first.getId() == id);


            for (int i = 1; i < data.size(); i++) {
                MoSchedule next = data.get(i);
                assertTrue(next.getId() == id);
                assertTrue(!next.getAddress().equals(first.getAddress()));
            }
        }


        List<MoSchedule> expected = service.getForId(540004);
        int actual = 1;

        assertTrue(actual == expected.size());


        List<MoSchedule> expected2 = service.getForId(540103);
        int actual2 = 4;

        assertTrue(actual2 == expected2.size());


        List<MoSchedule> expected3 = service.getForId(1);
        assertTrue(expected3.isEmpty());
    }*/
}