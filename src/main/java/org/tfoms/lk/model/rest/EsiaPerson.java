package org.tfoms.lk.model.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


@JsonIgnoreProperties(ignoreUnknown = true)
public class EsiaPerson {
    private String firstName;

    private String lastName;

    private String middleName;

    private String birthDate;

    private String gender;

    private boolean trusted;

    private String citizenship;

    private String snils;

    private Long updatedOn;

    private String status;

    private boolean verifying;

    private Long rIdDoc;

    private String containsUpCfmCode;

    private String eTag;



    public int getFullAge() throws ParseException {
        if(birthDate == null) return 0;

        LocalDate today = LocalDate.now();
        LocalDate dr = LocalDate.parse(birthDate, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        int years = today.getYear() -dr.getYear();

        if(dr.getDayOfYear() > today.getDayOfYear())
            years--;

        return years;
    }

    @Override
    public String toString() {
        return "EsiaPerson{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", gender='" + gender + '\'' +
                ", trusted=" + trusted +
                ", citizenship='" + citizenship + '\'' +
                ", snils='" + snils + '\'' +
                ", updatedOn=" + updatedOn +
                ", status='" + status + '\'' +
                ", verifying=" + verifying +
                ", rIdDoc=" + rIdDoc +
                ", containsUpCfmCode='" + containsUpCfmCode + '\'' +
                ", eTag='" + eTag + '\'' +
                '}';
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isTrusted() {
        return trusted;
    }

    public void setTrusted(boolean trusted) {
        this.trusted = trusted;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public Long getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Long updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isVerifying() {
        return verifying;
    }

    public void setVerifying(boolean verifying) {
        this.verifying = verifying;
    }

    public Long getrIdDoc() {
        return rIdDoc;
    }

    public void setrIdDoc(Long rIdDoc) {
        this.rIdDoc = rIdDoc;
    }

    public String getContainsUpCfmCode() {
        return containsUpCfmCode;
    }

    public void setContainsUpCfmCode(String containsUpCfmCode) {
        this.containsUpCfmCode = containsUpCfmCode;
    }

    public String geteTag() {
        return eTag;
    }

    public void seteTag(String eTag) {
        this.eTag = eTag;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getBirthDate() {
        return birthDate;
    }

}
