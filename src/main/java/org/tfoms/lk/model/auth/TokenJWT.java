package org.tfoms.lk.model.auth;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.Base64;
import java.util.Date;



public class TokenJWT {
    private Long nbf;
    private String scope;
    private String iss;

    @SerializedName("urn:esia:sid")
    private String sid;

    @SerializedName("urn:esia:sbj_id")
    private Long sbjId;

    private Long exp;

    private Long iat;

    private String client_id;


    private Date expires;
    private String encoded;
    private String refreshToken;

    public static TokenJWT getToken(String encodedToken){
        String[] jwtParts = encodedToken.split("\\.");
        //decode body
        String body = new String(Base64.getUrlDecoder().decode(jwtParts[1]));

        TokenJWT tokenJWT = new Gson().fromJson(body,TokenJWT.class);
        tokenJWT.encoded = encodedToken;

        return tokenJWT;
    }


    public boolean isValid(){
        return new Date().before(expires);
    }


    public void setEncoded(String encoded) {
        this.encoded = encoded;
    }

    public TokenJWT(){
    }

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public Long getNbf() {
        return nbf;
    }

    public String getScope() {
        return scope;
    }

    public String getIss() {
        return iss;
    }

    public String getSid() {
        return sid;
    }

    public Long getSbjId() {
        return sbjId;
    }

    public Long getExp() {
        return exp;
    }

    public Long getIat() {
        return iat;
    }

    public String getClient_id() {
        return client_id;
    }

    public String getEncoded() {
        return encoded;
    }

    @Override
    public String toString() {
        return "TokenJWT{" +
                "nbf=" + nbf +
                ", scope='" + scope + '\'' +
                ", iss='" + iss + '\'' +
                ", sid='" + sid + '\'' +
                ", sbjId=" + sbjId +
                ", exp=" + exp +
                ", iat=" + iat +
                ", client_id='" + client_id + '\'' +
                ", encoded='" + encoded + '\'' +
                '}';
    }
}