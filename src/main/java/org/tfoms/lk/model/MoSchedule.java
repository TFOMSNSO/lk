package org.tfoms.lk.model;



/**
 * Расписание рабоы мо
 * */
public class MoSchedule{
    private Integer id;
    private String name;
    private String address;
    private String weekdays;
    private String dayOffs;
    private String contacts;

    public MoSchedule(Integer id, String name, String address, String weekdays, String dayOffs, String contacts) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.weekdays = weekdays;
        this.dayOffs = dayOffs;
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return "Mo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", weekdays='" + weekdays + '\'' +
                ", dayOffs='" + dayOffs + '\'' +
                ", contacts='" + contacts + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWeekdays() {
        return weekdays;
    }

    public void setWeekdays(String weekdays) {
        this.weekdays = weekdays;
    }

    public String getDayOffs() {
        return dayOffs;
    }

    public void setDayOffs(String dayOffs) {
        this.dayOffs = dayOffs;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }
}
