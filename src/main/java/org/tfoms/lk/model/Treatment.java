package org.tfoms.lk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Treatment {
    @SerializedName("datBeg")
    private String dateRenderingFrom;
    @SerializedName("datEnd")
    private String dateRenderingTo;

    @SerializedName("type1")
    private String careRegimen;
    @SerializedName("type2")
    private String careType;
    @SerializedName("type3")
    private String name;

    @SerializedName("sum")
    private Double medServicesSum;

    @SerializedName("name")
    private String clinicName;
    @SerializedName("obl")
    private String regionName;

    @Override
    public String toString() {
        return "Treatment{" +
                "dateRenderingFrom=" + dateRenderingFrom +
                ", dateRenderingTo=" + dateRenderingTo +
                ", careRegimen='" + careRegimen + '\'' +
                ", careType='" + careType + '\'' +
                ", name='" + name + '\'' +
                ", medServicesSum=" + medServicesSum +
                ", clinicName='" + clinicName + '\'' +
                ", regionName='" + regionName + '\'' +
                '}';
    }

    public String getDateRenderingFrom() {
        return dateRenderingFrom;
    }

    public void setDateRenderingFrom(String dateRenderingFrom) {
        this.dateRenderingFrom = dateRenderingFrom;
    }

    public String getDateRenderingTo() {
        return dateRenderingTo;
    }

    public void setDateRenderingTo(String dateRenderingTo) {
        this.dateRenderingTo = dateRenderingTo;
    }

    public String getCareRegimen() {
        return careRegimen;
    }

    public void setCareRegimen(String careRegimen) {
        this.careRegimen = careRegimen;
    }

    public String getCareType() {
        return careType;
    }

    public void setCareType(String careType) {
        this.careType = careType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMedServicesSum() {
        return medServicesSum;
    }

    public void setMedServicesSum(Double medServicesSum) {
        this.medServicesSum = medServicesSum;
    }

    public String getClinicName() {
        return clinicName;
    }

    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
}
