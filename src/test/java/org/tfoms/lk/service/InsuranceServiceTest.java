package org.tfoms.lk.service;

import com.google.gson.Gson;
import org.apache.bcel.verifier.structurals.ExceptionHandler;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.tfoms.lk.model.DispInfo;
import org.tfoms.lk.model.Insurance;
import org.tfoms.lk.model.Treatment;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@RunWith(SpringRunner.class)
class InsuranceServiceTest {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    InsuranceService service;

    @Test
    public void testApi(){
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("regionCode", "54");//mock
        bodyMap.add("surname", "выдрин");
        bodyMap.add("firstname", "алексей");
        bodyMap.add("lastname", "эдуардович");
        bodyMap.add("datebythday", "27.12.1997");
        bodyMap.add("enp", "54");//mock
        bodyMap.add("dataFrom", "2018-12-12");
        bodyMap.add("dataEnd", "2019-12-12");

        HttpEntity<MultiValueMap<String,String>> httpEntity = new HttpEntity<>(bodyMap, headers);

        String list = restTemplate.postForObject("http://srv-guzm:7777/pls/webofoms/LK_NVFMS.info_worth",httpEntity,String.class);

        Treatment[] a = new Gson().fromJson(list.replace("[,","["),Treatment[].class);

        assertTrue( a.length > 0 );
    }

    @Test
    public void shouldReturnTreatment(){
        assertDoesNotThrow(() -> service.getTreatment(null,null,null,null,null,null));

        List<Treatment> badList = service.getTreatment("a","a","a","a","a","a");
        assertTrue(badList.isEmpty());

        List<Treatment> badList1 = service.getTreatment(null,null,null,null,null,null);
        assertTrue(badList1.isEmpty());

        List<Treatment> goodList = service.getTreatment("выдрин","Алексей","эдуардович","27.12.1997", "2018-01-01", "2019-01-01");
        assertTrue(!goodList.isEmpty());

        goodList = service.getTreatment("выдрин","Алексей","эдуардович","27.12.1997", "01.01.2019", "01.01.2020");
        assertTrue(!goodList.isEmpty());

        goodList = service.getTreatment("выдрин","Алексей","эдуардович","1997-12-27", "01.01.2019", "01.01.2020");
        assertTrue(!goodList.isEmpty());

        goodList = service.getTreatment("выдрин","Алексей","эдуардович","1997-12-27", "2019-01-01", "01.01.2020");
        assertTrue(!goodList.isEmpty());
    }


    @Test
    public void shouldReturnSomething(){
        assertDoesNotThrow(()-> service.getInsurance("a","a","a","b"));


        Insurance insurance = service.getInsurance("выдрин","алексей","эдуардович","27.12.1997");
        assertNotNull(insurance);


        insurance = service.getInsurance("a","d",null,"27.12.1997");
        assertNotNull(insurance);
    }

    @Test
    public void dispInfoTest(){
        //valid
        for(int i = 21; i < 100; i++){
            assertNotNull(service.getDispInfo("F",i));
            assertNotNull(service.getDispInfo("M",i));
        }


        DispInfo notValid = service.getDispInfo("asd",1);
        assertNotNull(notValid);
    }

}
