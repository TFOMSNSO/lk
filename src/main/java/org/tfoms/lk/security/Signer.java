package org.tfoms.lk.security;

import com.objsys.asn1j.runtime.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.stereotype.Component;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.*;
import ru.CryptoPro.JCP.ASN.PKIX1Explicit88.CertificateSerialNumber;
import ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Name;
import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCP.params.OID;

import java.io.IOException;
import java.security.*;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;


/**
 * Для подписывания сообщений
 * */
@Component
public class Signer {
    //алиас сертификата в хранилище ключей
    private static final String ALIAS = "ТФОМС НСО 2019 - копия";
    private static final char[] PASSWD = "123456Aa".toCharArray();
    private static final String PROVIDER = "JCP";
    private static final String STORE_TYPE = "HDImageStore";


    private final PrivateKey privateKey;
    private final X509Certificate certificate;

    public Signer() throws NoSuchProviderException, KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableKeyException {
        Security.addProvider(new BouncyCastleProvider());

        //загрузка KeyStore, приватного ключа и сертификата
        KeyStore keyStore = KeyStore.getInstance(STORE_TYPE, PROVIDER);
        keyStore.load(null,null);

        privateKey = (PrivateKey) keyStore.getKey(ALIAS, PASSWD);

        certificate = (X509Certificate)  keyStore.getCertificate(ALIAS);
    }


    /**
     * Создает подпись в формате PKCS#7 из сертификата и чистой(массив байт) подписи
     * @param cert сертификат подписи
     * @param sign подпись данных
     * @return подпись запроса в формате PKCS#7 detached signature
     * */
    public  byte[] createCMS(byte[] sign, Certificate cert) throws CertificateEncodingException, Asn1Exception, IOException {
        //oid ГОСТ 2012
        String digestOid = "1.2.643.2.2.9";
        String signOid = "1.2.643.2.2.19";


        final ContentInfo all = new ContentInfo();
        all.contentType = new Asn1ObjectIdentifier(
                new OID("1.2.840.113549.1.7.2").value);

        final SignedData cms = new SignedData();
        all.content = cms;
        cms.version = new CMSVersion(1);

        // digest
        cms.digestAlgorithms = new DigestAlgorithmIdentifiers(1);
        final DigestAlgorithmIdentifier a = new DigestAlgorithmIdentifier(
                new OID(digestOid).value);

        a.parameters = new Asn1Null();
        cms.digestAlgorithms.elements[0] = a;

        cms.encapContentInfo = new EncapsulatedContentInfo(
                new Asn1ObjectIdentifier(
                        new OID("1.2.840.113549.1.7.1").value), null);


        // certificate
        cms.certificates = new CertificateSet(1);
        final ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Certificate certificate =
                new ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Certificate();
        final Asn1BerDecodeBuffer decodeBuffer =
                new Asn1BerDecodeBuffer(cert.getEncoded());
        certificate.decode(decodeBuffer);

        cms.certificates.elements = new CertificateChoices[1];
        cms.certificates.elements[0] = new CertificateChoices();
        cms.certificates.elements[0].set_certificate(certificate);

        // signer info
        cms.signerInfos = new SignerInfos(1);
        cms.signerInfos.elements[0] = new SignerInfo();
        cms.signerInfos.elements[0].version = new CMSVersion(1);
        cms.signerInfos.elements[0].sid = new SignerIdentifier();

        final byte[] encodedName = ((X509Certificate) cert)
                .getIssuerX500Principal().getEncoded();
        final Asn1BerDecodeBuffer nameBuf = new Asn1BerDecodeBuffer(encodedName);
        final Name name = new Name();
        name.decode(nameBuf);

        final CertificateSerialNumber num = new CertificateSerialNumber(
                ((X509Certificate) cert).getSerialNumber());
        cms.signerInfos.elements[0].sid.set_issuerAndSerialNumber(
                new IssuerAndSerialNumber(name, num));
        cms.signerInfos.elements[0].digestAlgorithm =
                new DigestAlgorithmIdentifier(new OID(digestOid).value);
        cms.signerInfos.elements[0].digestAlgorithm.parameters = new Asn1Null();
        cms.signerInfos.elements[0].signatureAlgorithm =
                new SignatureAlgorithmIdentifier(new OID(signOid).value);
        cms.signerInfos.elements[0].signatureAlgorithm.parameters = new Asn1Null();
        cms.signerInfos.elements[0].signature = new SignatureValue(sign);

        // encode
        final Asn1BerEncodeBuffer asnBuf = new Asn1BerEncodeBuffer();
        all.encode(asnBuf, true);
        return asnBuf.getMsgCopy();
    }

    /**
     * – подпись запроса в формате PKCS#7 detached signature
     * @param data данные для подписи
     * @return detached signature
     * */
    public byte[] signData3(byte [] data) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, CertificateEncodingException, IOException, Asn1Exception {
        Signature signature = Signature.getInstance(JCP.GOST_DHEL_SIGN_NAME);
        signature.initSign(privateKey);
        signature.update(data);

        return createCMS(signature.sign(),certificate);
    }

}
