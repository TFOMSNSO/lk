package org.tfoms.lk.model.rest;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Documents {
    private String[] stateFacts;
    private Long size;
    private String eTag;
    private String[] elements;

    @Override
    public String toString() {
        return "Documents{" +
                "stateFacts=" + Arrays.toString(stateFacts) +
                ", size=" + size +
                ", eTag='" + eTag + '\'' +
                ", elements=" + Arrays.toString(elements) +
                '}';
    }

    public String[] getStateFacts() {
        return stateFacts;
    }

    public void setStateFacts(String[] stateFacts) {
        this.stateFacts = stateFacts;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String geteTag() {
        return eTag;
    }

    public void seteTag(String eTag) {
        this.eTag = eTag;
    }

    public String[] getElements() {
        return elements;
    }

    public void setElements(String[] elements) {
        this.elements = elements;
    }
}
