package org.tfoms.lk.export;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.tfoms.lk.model.Treatment;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;


public class ExcelBuilder {
    public static void treatmentsToExcel(String[][] treatments, OutputStream stream) throws IOException {
        String[] columns = {"Дата начала","Дата окончания","Наименование","Регион","Мед. учреждение","Тип лечения","Уровень оказания","Сумма лечения"};

        try(Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("Users");

            Font headerFont = workbook.createFont();

            headerFont.setBold(true);

            headerFont.setColor(IndexedColors.BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();

            headerCellStyle.setFont(headerFont);


            Row headerRow = sheet.createRow(0);

            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }


            int rowNum = 1;

            for (String[] treatment : treatments) {
                Row row = sheet.createRow(rowNum++);

                row.createCell(0).setCellValue(treatment[0]);
                row.createCell(1).setCellValue(treatment[1]);
                row.createCell(2).setCellValue(treatment[2]);
                row.createCell(3).setCellValue(treatment[3]);
                row.createCell(4).setCellValue(treatment[4]);
                row.createCell(5).setCellValue(treatment[5]);
                row.createCell(6).setCellValue(treatment[6]);
                row.createCell(7).setCellValue(treatment[7]);
            }


            for(int i =0; i < columns.length; i++){
                sheet.autoSizeColumn(i);
            }

            workbook.write(stream);
        }
    }


    /**
     * Формирует из списка лечений эксель файл и записывает его в поток
     * @param treatments список лечений
     * @param stream поток, куда запишется файл (пр. response.getOutpustStream())
     * */
    public static void treatmentToExcel(List<Treatment> treatments, OutputStream stream) throws IOException {
        String[] columns = {"Дата начала","Дата окончания","Наименование","Регион","Мед. учреждение","Тип лечения","Уровень оказания","Сумма лечения"};

        try(Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("Users");

            Font headerFont = workbook.createFont();

            headerFont.setBold(true);

            headerFont.setColor(IndexedColors.BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();

            headerCellStyle.setFont(headerFont);


            Row headerRow = sheet.createRow(0);

            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }


            int rowNum = 1;

            for (Treatment treatment : treatments) {
                Row row = sheet.createRow(rowNum++);

                row.createCell(0).setCellValue(treatment.getDateRenderingFrom());
                row.createCell(1).setCellValue(treatment.getDateRenderingTo());
                row.createCell(2).setCellValue(treatment.getName());
                row.createCell(3).setCellValue(treatment.getRegionName());
                row.createCell(4).setCellValue(treatment.getClinicName());
                row.createCell(5).setCellValue(treatment.getCareType());
                row.createCell(6).setCellValue(treatment.getCareRegimen());
                row.createCell(7).setCellValue(treatment.getMedServicesSum().toString());
            }


            for(int i =0; i < columns.length; i++){
                sheet.autoSizeColumn(i);
            }

            workbook.write(stream);
        }
    }
}
