package org.tfoms.lk.service;

import com.objsys.asn1j.runtime.Asn1Exception;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.tfoms.lk.model.auth.TokenJWT;
import org.tfoms.lk.model.auth.TokenWrap;
import org.tfoms.lk.model.rest.Documents;
import org.tfoms.lk.model.rest.EsiaPerson;
import org.tfoms.lk.security.Signer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

/**
 * Сервис, который посылает запросы к есиа
 * */
@Service
public class EsiaService {
    private static final Logger logger = LoggerFactory.getLogger(EsiaService.class);

    @Value("${esia.url.prns}")
    private String URL;

    @Value("${esia.url.prns.docs}")
    private String DOCS_URL;

    @Value("${esia.url.prns.addrs}")
    private String ADDRS_URL;

    @Value("${esia.url.prns.ctts}")
    private String CTTS_URL;

    @Value("${esia.url.te}")
    private String TOKEN_EXCHANGE_URL;




    @Value("${lk.auth.scopes}")
    private String SCOPE;

    @Value("${lk.clientid}")
    private String CLIENT_ID;

    private String TOKEN_TYPE= "Bearer";



    @Autowired
    private Signer signer;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    SimpleDateFormat format;
    /**
     * Метод получает информацию по человеку
     * @param accessToken токен доступа к есиа
     * @return экземпляр EsiaPerson
     * */
    public EsiaPerson getPersonInfo(TokenJWT accessToken) throws CertificateEncodingException, Asn1Exception, NoSuchAlgorithmException, IOException, SignatureException, InvalidKeyException {
        if(!accessToken.isValid()){
            refreshToken(accessToken);
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.setBearerAuth(accessToken.getEncoded());

        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity<EsiaPerson> responseEntity =
                restTemplate.exchange(URL + accessToken.getSbjId(),
                        HttpMethod.GET,
                        httpEntity,
                        EsiaPerson.class);

        return responseEntity.getBody();
    }
    /**
     * Запрашивает от есиа список с ссылками на документы
     * @param scope тип ресурсов:'addrs' - адресов, 'docs' - документы, 'ctts' - контакты и тд.
     * @param accessToken токен доступа к есиа
     * @return экземпляр Documents, в котором содержатся ссылки
     * */
    public Documents getDocumentList(String scope, TokenJWT accessToken){
        if(!accessToken.isValid()){
            try {
                refreshToken(accessToken);
            } catch (Exception e) {
                logger.error("Error while refreshing token: {}", e.toString());
            }
        }

        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.setBearerAuth(accessToken.getEncoded());

        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        String url = String.format(DOCS_URL,accessToken.getSbjId().toString());

        // 1-й кейс для наглядности
        switch (scope){
            case "docs": url = String.format(DOCS_URL, accessToken.getSbjId().toString()); break;
            case "addrs": url = String.format(ADDRS_URL, accessToken.getSbjId().toString()); break;
            case "ctts": url = String.format(CTTS_URL, accessToken.getSbjId().toString()); break;
        }


        ResponseEntity<Documents> responseEntity =
                restTemplate.exchange(url,
                        HttpMethod.GET,
                        httpEntity,
                        Documents.class);

        return responseEntity.getBody();
    }
    /**
     * Запрашивает от есиа ресурс по url
     * @param tClass класс ресурса
     * @param accessToken токен доступа к есиа
     * @param url URL по которому расположен ресурс
     * @return объект ресурса
     * */
    public <T> T getResourceByURL(String url, TokenJWT accessToken, Class<? extends T> tClass){
        if(!accessToken.isValid()){
            try {
                refreshToken(accessToken);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.setBearerAuth(accessToken.getEncoded());

        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity<? extends T> responseEntity =
                restTemplate.exchange(url,
                        HttpMethod.GET,
                        httpEntity,
                        tClass);

        return responseEntity.getBody();
    }
    /**
     * Для обновления маркера доступа
     * @param accessToken маркер доступа, кооторый обновляем
     * @return true - обновился, false - не обновился
     * */
    public boolean refreshToken(TokenJWT accessToken) throws IOException, Asn1Exception, NoSuchAlgorithmException, CertificateEncodingException, SignatureException, InvalidKeyException {

        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();

        String timestamp = format.format(new Date());
        String state = UUID.randomUUID().toString();

        String dataToSign = SCOPE + timestamp + CLIENT_ID + state;
        byte[] toBeSigned = dataToSign.getBytes("UTF-8");
        byte[] sign = signer.signData3(toBeSigned);

        String client_secret = Base64.getUrlEncoder().encodeToString(sign);

        bodyMap.add("client_id",CLIENT_ID);
        bodyMap.add("refresh_token",accessToken.getRefreshToken());
        bodyMap.add("grant_type","refresh_token");
        bodyMap.add("client_secret",client_secret);
        bodyMap.add("state",state);
        bodyMap.add("redirect_uri","https://lk.novofoms.ru/api/te");
        bodyMap.add("scope",SCOPE);
        bodyMap.add("timestamp",timestamp);
        bodyMap.add("token_type",TOKEN_TYPE);

        HttpEntity<MultiValueMap<String,String>> httpEntity = new HttpEntity<>(bodyMap, headers);


        try {
            TokenWrap response = restTemplate.postForObject(TOKEN_EXCHANGE_URL, httpEntity, TokenWrap.class);

            accessToken.setExpires(new Date(response.getExpires_in() * 1000 + new Date().getTime()));
            accessToken.setRefreshToken(response.getRefresh_token());
            accessToken.setEncoded(response.getAccess_token());
            return true;
        }catch (RestClientException e){
            logger.error("refreshToken error: " +  e.getMessage());
        }

        return false;

    }
}
