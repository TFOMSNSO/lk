package org.tfoms.lk.service;


import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.tfoms.lk.model.MoSchedule;


import java.io.IOException;
import java.util.*;

/**
 * in-memory database
 * */
@Service
public class MoScheduleService {
    private final static Logger logger = LoggerFactory.getLogger(MoScheduleService.class);

    private final TreeMap<Integer, List<MoSchedule>> SCHEDULES = new TreeMap<>();

    public MoScheduleService(){
        init("moSchedule.xlsx");
    }

    /**
     * Парсит excel файл с расписанием работы МО
     * и заполняет коллекцию schedules
     * @param filename название файла
     * */
    private void init(String filename){
        try(Workbook workbook = new XSSFWorkbook(new ClassPathResource(filename).getInputStream())){
            Sheet sheet = workbook.getSheetAt(0);
            //
            List<CellRangeAddress> ls = sheet.getMergedRegions();

            TreeMap<Integer, CellRangeAddress> mos = new TreeMap<>();

            for(CellRangeAddress cell : ls){
                if(cell.getFirstColumn() ==  cell.getLastColumn() && cell.getLastColumn() == 1){//берем rowspan'ы только второго столбца
                    //mo - код МО
                    int mo =(int)(sheet.getRow(cell.getFirstRow()).getCell(cell.getFirstColumn()).getNumericCellValue());
                    mos.put(mo,cell);
                }
            }


            for(int i = 2; i < sheet.getPhysicalNumberOfRows() - 1; i++){
                Row row = sheet.getRow(i);
                Integer id = (int) row.getCell(1).getNumericCellValue();

                if(!mos.containsKey(id)){
                    MoSchedule mo = new MoSchedule(id,
                            row.getCell(2).getStringCellValue(),
                            row.getCell(3).getStringCellValue(),
                            row.getCell(4).getStringCellValue(),
                            row.getCell(5).getStringCellValue(),
                            row.getCell(6).getStringCellValue());

                    SCHEDULES.put(id, Arrays.asList(mo));
                }else{
                    int count = mos.get(id).getLastRow() - mos.get(id).getFirstRow();
                    String name = row.getCell(2).getStringCellValue();
                    List<MoSchedule> temp = new ArrayList<>();
                    temp.add(new MoSchedule(id,name,
                            row.getCell(3).getStringCellValue(),
                            row.getCell(4).getStringCellValue(),
                            row.getCell(5).getStringCellValue(),
                            row.getCell(6).getStringCellValue()));

                    while(count != 0){
                        i++;
                        count--;
                        row = sheet.getRow(i);
                        temp.add(new MoSchedule(id,name,
                                row.getCell(3).getStringCellValue(),
                                row.getCell(4).getStringCellValue(),
                                row.getCell(5).getStringCellValue(),
                                row.getCell(6).getStringCellValue()));

                    }

                    SCHEDULES.put(id,temp);
                }
            }


            logger.info("MoScheduleService init OK");
        } catch (IOException e) {
            logger.error("MoScheduleService init:" + e);

        }
    }
    /**
     * Получить список адресов с расписанием
     * */
    public List<MoSchedule> getForId(Integer id){
        if(id != null && SCHEDULES.containsKey(id)){
            return SCHEDULES.get(id);
        }else {
            return Collections.emptyList();
        }
    }
}
