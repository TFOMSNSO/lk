package org.tfoms.lk.model.rest;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Document {
    private String[] stateFacts;
    private Long id;
    private String type;
    private String vrfStu;
    private String series;
    private String number;
    private String issueDate;
    private String issueId;
    private String issuedBy;
    private String eTag;


    @Override
    public String toString() {
        return "Document{" +
                "stateFacts=" + Arrays.toString(stateFacts) +
                ", id=" + id +
                ", type='" + type + '\'' +
                ", vrfStu='" + vrfStu + '\'' +
                ", series='" + series + '\'' +
                ", number='" + number + '\'' +
                ", issueDate='" + issueDate + '\'' +
                ", issueId='" + issueId + '\'' +
                ", issuedBy='" + issuedBy + '\'' +
                ", eTag='" + eTag + '\'' +
                '}';
    }

    public String[] getStateFacts() {
        return stateFacts;
    }

    public void setStateFacts(String[] stateFacts) {
        this.stateFacts = stateFacts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVrfStu() {
        return vrfStu;
    }

    public void setVrfStu(String vrfStu) {
        this.vrfStu = vrfStu;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    public String geteTag() {
        return eTag;
    }

    public void seteTag(String eTag) {
        this.eTag = eTag;
    }
}
