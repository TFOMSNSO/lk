package org.tfoms.lk.service;


import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.tfoms.lk.model.DispInfo;
import org.tfoms.lk.model.Insurance;
import org.tfoms.lk.model.Treatment;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * Сервис, который посылает запросы к БД
 * (на бд rest api)
 * */
@Service
public class InsuranceService {
    private static final Logger logger = LoggerFactory.getLogger(InsuranceService.class);

    @Value("${service.insurance.url}")
    private String INSURANCE_URL;

    @Value("${service.treatment.url}")
    private String TREATMENT_URL;

    @Value("${service.disp-info.url}")
    private String DISP_INFO_URL;

    @Autowired
    private RestTemplate restTemplate;


    private final DispInfo emptyDispInfo = DispInfo.getEmpty();

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");


    /**
     * Запрос мед. страховки по ФИОДу
     * @return мед. страховка
     * */
    public Insurance getInsurance(String fam, String im, String ot, String dr) {
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();

        bodyMap.add("vfam", fam);
        bodyMap.add("vim", im);
        bodyMap.add("vot", ot);
        bodyMap.add("vdr", dr);

        HttpEntity<MultiValueMap<String,String>> httpEntity = new HttpEntity<>(bodyMap, headers);
        try {
            String json = restTemplate.postForObject(INSURANCE_URL, httpEntity, String.class);
            return new Gson().fromJson(json, Insurance.class);
        } catch (RestClientException ex){
            logger.error("GetInsurance error: message={}, FIOD={} {} {} {}, url={}",ex.getMessage(),fam,im,ot,dr, INSURANCE_URL);
            return new Insurance();
        }
    }

    /**
     * Узнать объем диспансеризации
     * @param gender пол "F" или "M"
     * @param age возраст
     * */
    public DispInfo getDispInfo(String gender, int age){
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();

        bodyMap.add("inAge", age + "");
        bodyMap.add("inGender", gender);

        HttpEntity<MultiValueMap<String,String>> httpEntity = new HttpEntity<>(bodyMap, headers);

        try {
            String json =  restTemplate.postForObject(DISP_INFO_URL, httpEntity, String.class);
            return new Gson().fromJson(json, DispInfo.class);
        } catch (RestClientException ex){
            logger.error("getDispInfo error: message={}, gender={}, age={}",ex.getMessage(),gender,age);
            return emptyDispInfo;
        }
    }




    /**
     * Запрос информации о лечении по фиоду за определенный период
     * @param dateBegin дата начала, с которой ищем
     * @param dateEnd дата окончания, до которой ищем
     * @return список лечений
     * */
    public List<Treatment> getTreatment(String fam, String im, String ot, String dr, String dateBegin, String dateEnd){
        if(dateBegin!= null && dateBegin.matches("\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d")){ // to yyyy-MM-dd
            dateBegin = LocalDate.parse(dateBegin,formatter).toString();
        }
        if(dateEnd != null && dateEnd.matches("\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d")){ // to yyyy-MM-dd
            dateEnd = LocalDate.parse(dateEnd,formatter).toString();
        }
        if(dr != null && dr.matches("\\d\\d\\d\\d-\\d\\d-\\d\\d")){ // to dd.MM.yyyy
            dr = LocalDate.parse(dr).format(formatter);
        }


        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("regionCode", "54");//mock
        bodyMap.add("surname", fam);
        bodyMap.add("firstname", im);
        bodyMap.add("lastname", String.valueOf(ot));
        bodyMap.add("datebythday", String.valueOf(dr));
        bodyMap.add("enp", "54");//mock
        bodyMap.add("dataFrom", dateBegin);
        bodyMap.add("dataEnd", dateEnd);


        HttpEntity<MultiValueMap<String,String>> httpEntity = new HttpEntity<>(bodyMap, headers);


        try {
            String list = restTemplate.postForObject(TREATMENT_URL, httpEntity, String.class);

            // там на ендпоинте ошибка
            if(list.startsWith("[,")) {
                list = list.replace("[,", "[");
            }
            return Arrays.asList(new Gson().fromJson(list,Treatment[].class));
        } catch (RestClientException e){
            logger.error("getTreatment error: message={}, FIOD={} {} {} {}, dataFrom={}, dataEnd={}",
                    e.getMessage(),fam,im,ot,dr,dateBegin,dateEnd);
            return Collections.emptyList();
        }
    }

}
