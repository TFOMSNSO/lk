package org.tfoms.lk.controllers;


import com.objsys.asn1j.runtime.Asn1Exception;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.tfoms.lk.export.ExcelBuilder;
import org.tfoms.lk.model.DispInfo;
import org.tfoms.lk.model.Insurance;
import org.tfoms.lk.model.Treatment;
import org.tfoms.lk.model.auth.TokenJWT;
import org.tfoms.lk.model.rest.*;
import org.tfoms.lk.service.EsiaService;
import org.tfoms.lk.service.InsuranceService;
import org.tfoms.lk.service.MoScheduleService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


@Controller
public class MainConroller {
    private static Logger logger = LoggerFactory.getLogger(MainConroller.class);


    @Autowired
    EsiaService esiaService;

    @Autowired
    InsuranceService insuranceService;

    @Autowired
    MoScheduleService moScheduleService;


    @RequestMapping("/")
    public String index(HttpServletRequest request) {
        TokenJWT tokenJWT = (TokenJWT) request.getSession().getAttribute("access_token");

        //Если пользователь уже авторизован
        if(tokenJWT != null){
            return "forward:/info";
        }

        return "index";
    }


    /**
     * Поиск лечений человека за определенный период
     * @param dateBegin дата начала
     * @param dateEnd дата окончания
     * @return view
     * */
    @RequestMapping("/treat")
    public String treatment(HttpServletRequest request, @RequestParam String dateBegin
            , @RequestParam String dateEnd, Model model) throws Exception {

        TokenJWT tokenJWT = (TokenJWT) request.getSession().getAttribute("access_token");

        if(tokenJWT == null){
            return "redirect:/";
        }

        //получаем данные человека из есиа
        EsiaPerson person = esiaService.getPersonInfo(tokenJWT);

        List<Treatment> treatments = insuranceService.getTreatment(person.getLastName(),person.getFirstName(),
                person.getMiddleName(),person.getBirthDate(),dateBegin,dateEnd);

        model.addAttribute("personInfo",person);

        if(!treatments.isEmpty()) {
            model.addAttribute("treats", treatments);
        }

        model.addAttribute("dateBegin",dateBegin);
        model.addAttribute("dateEnd",dateEnd);


        return "treat";
    }

    /**
     * Метод посылает запросы к есиа и получает информацию о человеке
     * @return model and view(с данными о человеке)
     * */
    @RequestMapping("/info")
    public String info(HttpServletRequest request, Model model)
            throws Asn1Exception, NoSuchAlgorithmException, CertificateEncodingException, SignatureException, InvalidKeyException, IOException, ParseException {
        TokenJWT tokenJWT = (TokenJWT) request.getSession().getAttribute("access_token");

        if(tokenJWT == null) {
            return "redirect:/";
        }

        //получаем данные человека из есиа
        EsiaPerson person = esiaService.getPersonInfo(tokenJWT);

        //получаем список документов
        Documents docs = esiaService.getDocumentList("docs", tokenJWT);

        //получаем список адресов
        Documents addrs = esiaService.getDocumentList("addrs", tokenJWT);

        //получаем список контактов
        Documents ctts = esiaService.getDocumentList("ctts", tokenJWT);

        List<Document> documents = new ArrayList<>();
        List<Address> addresses = new ArrayList<>();
        List<Contact> contacts = new ArrayList<>();

        //загружаем по очереди каждый документ
        for(String element : docs.getElements()){
            documents.add(esiaService.getResourceByURL(element, tokenJWT, Document.class));
        }

        //загружаем по очереди каждый адрес
        for(String element : addrs.getElements()){
            addresses.add(esiaService.getResourceByURL(element, tokenJWT, Address.class));
        }

        //загружаем по очереди каждый контакт
        for(String element : ctts.getElements()){
            contacts.add(esiaService.getResourceByURL(element, tokenJWT, Contact.class));
        }

        //данные о страховании
        Insurance insurance = insuranceService.getInsurance(person.getLastName().toUpperCase(),
                    person.getFirstName().toUpperCase(),
                    person.getMiddleName() == null ? "-" : person.getMiddleName(),
                    person.getBirthDate() == null ? "" : person.getBirthDate());


        //получаем объем диспансеризации
        DispInfo dispInfo = insuranceService.getDispInfo(person.getGender(), person.getFullAge());

        List<String> dispNames = dispInfo.parseNames();
        List<String> dispPrims = dispInfo.parsePrims();


        model.addAttribute("dispNames", dispNames);
        model.addAttribute("dispPrims", dispPrims);
        model.addAttribute("dispInfo", dispInfo);

        //Добавляем в модель пасспорт
        if(insurance.getDocid() != null && insurance.getSerDoc() != null && insurance.getNumDoc() != null) {
            for (Document document : documents) {
                if (document.getType().equalsIgnoreCase("RF_PASSPORT") && insurance.getDocid() == 5) {
                    if (!insurance.getNumDoc().trim().replaceAll(" ", "").equals(document.getNumber())
                            || !insurance.getSerDoc().trim().replaceAll(" ", "").equals(document.getSeries())) {
                        model.addAttribute("passportEquals", false);
                    } else {
                        model.addAttribute("passportEquals", true);
                    }
                    model.addAttribute("passport", document);
                }
            }
        }
        //Добавляем в модель регистарацию мо месту жительства(с госуслуг)
        for(Address address : addresses){
            if(address.getType().equalsIgnoreCase("PLV")){
                model.addAttribute("address", address);
            }
        }

        //Добавляем в модель email
        for(Contact contact : contacts){
            if(contact.getType().equalsIgnoreCase("EML")){
                model.addAttribute("email", contact);
            }
        }


        model.addAttribute("personInfo",person);

        //insurance - данные о мед. страховке
        model.addAttribute("personInsurance",insurance);

        //график работы лпу прикрепления
        model.addAttribute("moSchedule",moScheduleService.getForId(insurance.getLpuId()));

        return "info";
    }


    /**
     * Экспорт таблицы с лечением в эксель
     * */
    @RequestMapping("/export")
    public void exportToExcel(HttpServletRequest request, HttpServletResponse response
            ,@RequestParam String dateFrom,@RequestParam String dateTo) throws Asn1Exception, NoSuchAlgorithmException
            , CertificateEncodingException, SignatureException, InvalidKeyException, IOException {
        TokenJWT tokenJWT = (TokenJWT) request.getSession().getAttribute("access_token");

        if(tokenJWT != null){
            EsiaPerson person = esiaService.getPersonInfo(tokenJWT);

            List<Treatment> treatments = insuranceService.getTreatment(person.getLastName(),person.getFirstName(),
                    person.getMiddleName(),person.getBirthDate(),dateFrom,dateTo);


            response.addHeader("Content-Disposition", "attachment; filename=Treats.xlsx");
            try {
                ExcelBuilder.treatmentToExcel(treatments, response.getOutputStream());
            } catch (IOException e){
                logger.error("Write to excel error: message={}, dateFrom={}, dateTo={}, FIOD={} {} {} {}",e.getMessage(), dateFrom, dateTo,
                        person.getLastName(), person.getFirstName(), person.getMiddleName(), person.getBirthDate());
                throw new IOException("Не удалось записать в файл.");
            }
        }
    }



    @RequestMapping("favicon.ico")
    public String favicon() {
        return "forward:/images/favicon.ico";
    }

    @RequestMapping("/agreement.doc")
    public String agreement() {
        return "forward:/docs/agreement.doc";
    }
}

