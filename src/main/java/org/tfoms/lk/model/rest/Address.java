package org.tfoms.lk.model.rest;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {
    private String[] stateFacts;
    private Long id;
    private String type;
    private String addressStr;
    private String fiasCode;
    private String flat;
    private String countryId;
    private String house;
    private String building;
    private String frame;
    private String city;
    private String district;
    private String area;
    private String settlement;
    private String additionArea;
    private String additionAreaStreet;
    private String zipCode;
    private String street;
    private String region;
    private String eTag;


    @Override
    public String toString() {
        return "Address{" +
                "stateFacts=" + Arrays.toString(stateFacts) +
                ", id=" + id +
                ", type='" + type + '\'' +
                ", addressStr='" + addressStr + '\'' +
                ", fiasCode='" + fiasCode + '\'' +
                ", flat='" + flat + '\'' +
                ", countryId='" + countryId + '\'' +
                ", house='" + house + '\'' +
                ", building='" + building + '\'' +
                ", frame='" + frame + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", area='" + area + '\'' +
                ", settlement='" + settlement + '\'' +
                ", additionArea='" + additionArea + '\'' +
                ", additionAreaStreet='" + additionAreaStreet + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", street='" + street + '\'' +
                ", region='" + region + '\'' +
                ", eTag='" + eTag + '\'' +
                '}';
    }

    public String getFrame() {
        return frame;
    }

    public void setFrame(String frame) {
        this.frame = frame;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSettlement() {
        return settlement;
    }

    public void setSettlement(String settlement) {
        this.settlement = settlement;
    }

    public String getAdditionArea() {
        return additionArea;
    }

    public void setAdditionArea(String additionArea) {
        this.additionArea = additionArea;
    }

    public String getAdditionAreaStreet() {
        return additionAreaStreet;
    }

    public void setAdditionAreaStreet(String additionAreaStreet) {
        this.additionAreaStreet = additionAreaStreet;
    }

    public String[] getStateFacts() {
        return stateFacts;
    }

    public void setStateFacts(String[] stateFacts) {
        this.stateFacts = stateFacts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddressStr() {
        return addressStr;
    }

    public void setAddressStr(String addressStr) {
        this.addressStr = addressStr;
    }

    public String getFiasCode() {
        return fiasCode;
    }

    public void setFiasCode(String fiasCode) {
        this.fiasCode = fiasCode;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String geteTag() {
        return eTag;
    }

    public void seteTag(String eTag) {
        this.eTag = eTag;
    }
}
