package org.tfoms.lk.model;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DispInfo {
    private String gender;
    private Integer age;
    private String prims;
    private String names;

    private static DispInfo empty = new DispInfo();
    static {
        empty.setAge(0);
    }

    @Override
    public String toString() {
        return "DispInfo{" +
                "gender='" + gender + '\'' +
                ", age=" + age +
                ", prims='" + prims + '\'' +
                ", names='" + names + '\'' +
                '}';
    }


    public static DispInfo getEmpty(){
        return empty;
    }

    public List<String> parseNames(){
        if(names == null || names.equals("")){
            return Collections.emptyList();
        }
        ArrayList<String> result = new ArrayList<>(Arrays.asList(names.split("\\|")));
        if(result.get(0).equals("")){
            result.remove(0);
        }

        return result;
    }

    public List<String> parsePrims(){
        if(prims == null || prims.equals("")){
            return Collections.emptyList();
        }

        return new ArrayList<>(Arrays.asList(prims.split("\\|")));
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPrims() {
        return prims;
    }

    public void setPrims(String prims) {
        this.prims = prims;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }
}
